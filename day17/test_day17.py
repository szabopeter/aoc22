import unittest

from day17.day17 import load_input


class Day17TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(3068, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(3059, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(1514285714288, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(1500874635587, sut.part2())


if __name__ == "__main__":
    unittest.main()
