import logging
from pathlib import Path
from typing import Iterable

from shared.point import Point, Vector


class Shape:
    def __init__(self, rocks: set[Vector]):
        self.rocks = rocks.copy()
        self.width = 1 + max(rock.x for rock in rocks)
        self.height = 1 + max(rock.y for rock in rocks)


class PositionedShape:
    def __init__(self, position: Vector, shape: Shape):
        self.shape = shape
        self.position = position


def get_rock_pos(cp: Vector, rps: set[Vector]) -> set[Vector]:
    return {Point(cp.x + rp.x, cp.y - rp.y) for rp in rps}


def is_overlapping(a: set, b: Iterable) -> bool:
    return not a.isdisjoint(b)


class Day17:
    def __init__(self, jet_pattern: str, shapes: Iterable[Shape]):
        self.jet_xds = [{">": 1, "<": -1}[ch] for ch in jet_pattern]
        self.jet_index = 0
        self.shapes = list(shapes)
        self.shape_index = 0
        self.rocks: set[Vector] = set()
        self.width = 7
        self.seen = {}

    def part1(self) -> int:
        return self.calculate(2022)

    def part2(self) -> int:
        return self.calculate(1000000000000)

    def calculate(self, target):
        height = 0
        height_increases = []
        for i in range(target):
            top_row_count = 2 ** 2 ** 2
            displacement = Point(0, top_row_count - height)
            top_rows = tuple(sorted(
                rock + displacement
                for rock in self.rocks
                if rock.y >= height - top_row_count
            ))
            key = (self.shape_index, self.jet_index, top_rows)
            last_i = self.seen.get(key)
            if last_i is None:
                shape = self.get_next_shape()
                self.place(shape)
                height_increase = self.get_height() - height
                height_increases.append(height_increase)
                self.seen[key] = i
                height += height_increase
                continue

            delta_i = i - last_i
            logging.info(
                "Will use results from %d for %d (Δi=%d)", last_i, i, delta_i)
            height_increase_index = i - delta_i
            cycle_height_increase = sum(height_increases[i - delta_i:i])
            cycle_count = (target - i) // delta_i
            height += cycle_height_increase * cycle_count
            j = i + cycle_count * delta_i
            while j < target:
                height_increase = height_increases[height_increase_index]
                height_increase_index += 1
                if height_increase_index >= len(height_increases):
                    height_increase_index -= delta_i
                height += height_increase
                j += 1
            break
        return height

    def get_height(self):
        return 1 + max((rock.y for rock in self.rocks), default=-1)

    def get_next_shape(self) -> Shape:
        shape = self.shapes[self.shape_index]
        self.shape_index += 1
        self.shape_index %= len(self.shapes)
        return shape

    def get_next_jet_xd(self) -> int:
        xd = self.jet_xds[self.jet_index]
        self.jet_index += 1
        self.jet_index %= len(self.jet_xds)
        return xd

    def place(self, shape: Shape) -> None:
        y = self.get_height() + 3 + shape.height - 1
        x = 2
        pshape = PositionedShape(Point(x, y), shape)

        is_still_falling = True
        while is_still_falling:
            pshape = self.apply_jet(pshape)
            pshape, is_still_falling = self.apply_fall(pshape)

        new_rocks = get_rock_pos(pshape.position, pshape.shape.rocks)
        assert not is_overlapping(self.rocks, new_rocks)
        self.rocks.update(new_rocks)

    def apply_jet(self, pshape: PositionedShape) -> PositionedShape:
        xd = self.get_next_jet_xd()
        new_x = pshape.position.x + xd
        if 0 <= new_x <= self.width - pshape.shape.width:
            new_pos = Point(new_x, pshape.position.y)
            rocks = get_rock_pos(new_pos, pshape.shape.rocks)
            if not is_overlapping(self.rocks, rocks):
                pshape = PositionedShape(new_pos, pshape.shape)
        return pshape

    def apply_fall(self, pshape: PositionedShape) -> tuple[PositionedShape, bool]:
        new_pos = pshape.position + Point(0, -1)
        rocks = get_rock_pos(new_pos, pshape.shape.rocks)
        hit_rock_bottom = new_pos.y < pshape.shape.height - 1
        is_placed = hit_rock_bottom or is_overlapping(self.rocks, rocks)
        if is_placed:
            return pshape, False

        return PositionedShape(new_pos, pshape.shape), True

    def __str__(self) -> str:
        h = self.get_height()
        return "\n".join(
            "".join("#" if Point(x, y)
                    in self.rocks else "." for x in range(self.width))
            for y in range(h - 1, 0, -1)
        )


def load_input(filename="input.txt") -> Day17:
    file = Path(__file__).with_name(filename)
    jet_pattern = file.read_text().strip()
    file = Path(__file__).with_name("shapes.txt")
    paras = file.read_text().split("\n\n")
    assert paras, f"{filename} is empty!"
    return Day17(jet_pattern, map(parse_shape, paras))


def parse_shape(para: str) -> Shape:
    lines = para.splitlines()
    return Shape({
        Point(x, y)
        for y, line in enumerate(lines)
        for x, ch in enumerate(line)
        if ch == "#"
    })
