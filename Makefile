all: isort autopep8

isort:
	isort .

autopep8:
	autopep8 --in-place --recursive --exclude venv .
