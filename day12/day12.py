from pathlib import Path
from typing import Iterable

from shared.point import Point, Vector

dirs = {
    "U": Point(0, -1),
    "D": Point(0, +1),
    "L": Point(-1, 0),
    "R": Point(+1, 0),
}


class Day12:
    def __init__(self, lines: list[str]):
        self.h = len(lines)
        self.w = len(lines[0])
        assert all(len(line) == self.w for line in lines)
        self.pts = pts = {
            Point(x, y): lines[y][x] for x in range(self.w) for y in range(self.h)
        }

        def find(ch: str) -> Vector:
            hits = [pt for pt, c in pts.items() if c == ch]
            assert len(hits) == 1
            return hits.pop(0)

        self.start = find("S")
        self.end = find("E")

    def part1(self) -> int:
        edges = {
            (pt1, pt2)
            for pt1 in self.pts for pt2 in self.get_next(pt1)
            if self.can_go(pt1, pt2)
        }
        g = Graph(edges)
        shortest = g.find_shortest_path(self.start, {self.end})
        return len(shortest) - 1

    def part2(self) -> int:
        edges = {
            (pt1, pt2)
            for pt1 in self.pts for pt2 in self.get_next(pt1)
            if self.can_go(pt2, pt1)
        }
        g = Graph(edges)
        all_a = {pt for pt, ch in self.pts.items() if ch in "aS"}
        shortest = g.find_shortest_path(self.end, all_a)
        return len(shortest) - 1

    def get_next(self, pt: Vector) -> Iterable[Vector]:
        for dv in dirs.values():
            nxt = pt + dv
            if nxt not in self.pts:
                continue
            yield nxt

    def can_go(self, pt1: Vector, pt2: Vector) -> bool:
        def get_h(pt: Vector) -> int:
            ch = self.pts[pt]
            if ch == "S":
                ch = "a"
            elif ch == "E":
                ch = "z"
            return ord(ch)
        h1 = get_h(pt1)
        h2 = get_h(pt2)
        return h2 <= h1 + 1


class Graph:
    def __init__(self, edges: Iterable[tuple]):
        self.edges = list(edges)
        assert all(len(edge) == 2 for edge in self.edges)
        self.can_go = {}
        for n1, n2 in self.edges:
            self.can_go[n1] = self.can_go.get(n1, set()).union([n2])

    def find_shortest_path(self, start, end: set) -> list:
        to_process = [[start]]
        visited = {start}
        while to_process:
            path = to_process.pop(0)
            assert isinstance(path, list)
            for nxt in self.can_go.get(path[-1], set()):
                if nxt in visited:
                    continue

                if nxt in end:
                    return path + [nxt]
                to_process.append(path + [nxt])
                visited.add(nxt)
        raise ValueError("No path found =(")


def load_input(filename="input.txt") -> Day12:
    file = Path(__file__).with_name(filename)
    lines = file.read_text().splitlines()
    assert lines, f"{filename} is empty!"
    return Day12(lines)
