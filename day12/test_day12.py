import unittest

from day12.day12 import load_input


class Day12TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(31, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(517, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(29, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(512, sut.part2())


if __name__ == "__main__":
    unittest.main()
