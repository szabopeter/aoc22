from typing import Iterable


class Vector:
    def __init__(self, values: Iterable[int]):
        self.values = tuple(values)
        self.x = self.values[0]
        self.y = self.values[1] if len(self.values) >= 2 else 0
        self.z = self.values[2] if len(self.values) >= 3 else 0

    def __len__(self) -> int:
        return len(self.values)

    def assert_compatibility_with(self, other) -> "Vector":
        assert isinstance(other, Vector)
        assert len(self) == len(other)
        return other

    def clone(self):
        return Vector(self.values)

    def __eq__(self, other: object) -> bool:
        return self.values == self.assert_compatibility_with(other).values

    def __hash__(self) -> int:
        return hash(self.values)

    def __str__(self) -> str:
        vals = ",".join(str(v) for v in self.values)
        return f"V({vals})"

    def __repr__(self) -> str:
        return str(self)

    def __neg__(self) -> "Vector":
        return Vector(-v for v in self.values)

    def __add__(self, other: "Vector") -> "Vector":
        self.assert_compatibility_with(other)
        return Vector(v1 + v2 for v1, v2 in zip(self.values, other.values))

    def __sub__(self, other: "Vector") -> "Vector":
        return self + (-other)

    def __lt__(self, other: "Vector") -> bool:
        self.assert_compatibility_with(other)
        return self.values < other.values

    def mdist(self) -> int:
        return sum(abs(v) for v in self.values)

    def unit(self) -> "Vector":
        """
            >>> Vector([2, 0]).unit() == Vector([1, 0])
            True
            >>> Vector([0, -4]).unit() == Vector([0, -1])
            True
            >>> Vector([3, -3]).unit() == Vector([1, -1])
            True
        """
        nonzeroes = set(abs(v) for v in self.values).difference([0])
        assert len(nonzeroes) == 1
        nonzero = list(nonzeroes)[0]
        return Vector(v // nonzero for v in self.values)


def Point3d(x: int, y: int, z: int) -> Vector:
    return Vector((x, y, z))


def Point(x: int, y: int) -> Vector:
    return Vector((x, y))
