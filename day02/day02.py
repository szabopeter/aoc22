import enum
from pathlib import Path
from typing import Union

NEED_LOSE = "X"
NEED_DRAW = "Y"
NEED_WIN = "Z"


class Silly(enum.IntEnum):
    Rock = 0
    Paper = 1
    Scissors = 2

    @staticmethod
    def parse(letter: str) -> "Silly":
        return {
            "A": Silly.Rock,
            "B": Silly.Paper,
            "C": Silly.Scissors,
            "X": Silly.Rock,
            "Y": Silly.Paper,
            "Z": Silly.Scissors,
        }[letter]

    def beats(self, other: "Silly") -> bool:
        return Silly.what_beats(other) == self

    @staticmethod
    def what_beats(target: "Silly") -> "Silly":
        return {
            Silly.Rock: Silly.Paper,
            Silly.Paper: Silly.Scissors,
            Silly.Scissors: Silly.Rock,
        }[target]

    @staticmethod
    def what_loses_to(target: "Silly") -> "Silly":
        return {
            Silly.Rock: Silly.Scissors,
            Silly.Paper: Silly.Rock,
            Silly.Scissors: Silly.Paper,
        }[target]


class Day02:
    def __init__(self, rules: list[tuple[str, ...]]):
        assert all(len(rule) == 2 for rule in rules)
        self.rules = rules

    def part1(self) -> int:
        return sum(Rule(opp, me).score() for opp, me in self.rules)

    def part2(self):
        def make_rule(opp_letter: str, outcome: str) -> Rule:
            opp = Silly.parse(opp_letter)
            if outcome == NEED_LOSE:
                me = Silly.what_loses_to(opp)
            elif outcome == NEED_DRAW:
                me = opp
            else:
                assert outcome == NEED_WIN
                me = Silly.what_beats(opp)
            return Rule(opp, me)

        return sum(make_rule(opp, outcome).score() for opp, outcome in self.rules)


class Rule:
    def __init__(self, opp: Union[str, Silly], me: Union[str, Silly]):
        self.opp = Silly.parse(opp) if isinstance(opp, str) else opp
        self.me = Silly.parse(me) if isinstance(me, str) else me

    def score(self) -> int:
        score_for_outcome = 0
        if self.opp == self.me:
            score_for_outcome = 3
        elif self.me.beats(self.opp):
            score_for_outcome = 6
        else:
            assert self.opp.beats(self.me)

        score_for_shape = {
            Silly.Rock: 1,
            Silly.Paper: 2,
            Silly.Scissors: 3,
        }[self.me]

        return score_for_outcome + score_for_shape


def load_input(filename="input.txt") -> Day02:
    file = Path(__file__).with_name(filename)
    lines = [tuple(line.split()) for line in file.read_text().splitlines()]
    return Day02(lines)
