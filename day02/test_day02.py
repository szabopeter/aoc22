import unittest

from day02.day02 import Day02, load_input


class Day02TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = get_example()
        self.assertEqual(15, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertNotEqual(13531, sut.part1())
        self.assertEqual(11449, sut.part1())

    def test_part2_example(self):
        sut = get_example()
        self.assertEqual(12, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(13187, sut.part2())


def get_example():
    return Day02(
        [
            ("A", "Y"),
            ("B", "X"),
            ("C", "Z"),
        ]
    )


if __name__ == "__main__":
    unittest.main()
