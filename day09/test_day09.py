import unittest

from day09.day09 import load_input


class Day09TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(13, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertNotEqual(6521, sut.part1())
        self.assertEqual(6522, sut.part1())

    def test_part2_example(self):
        sut = load_input("example2.txt")
        self.assertEqual(36, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(2717, sut.part2())


if __name__ == "__main__":
    unittest.main()
