from pathlib import Path
from typing import Iterable

from shared.point import Point, Vector

dirs = {
    "U": Point(0, -1),
    "D": Point(0, +1),
    "L": Point(-1, 0),
    "R": Point(+1, 0),
}


def do_move(p: Vector, move: Vector) -> Vector:
    return p + move


def get_following_position(head: Vector, tail: Vector) -> Vector:
    d = head - tail
    new_pos = Point(tail.x + sgn(d.x), tail.y + sgn(d.y))
    if new_pos == head:
        return tail
    return new_pos


def sgn(v: int) -> int:
    if v > 0:
        return 1
    if v < 0:
        return -1
    assert v == 0
    return 0


class Day09:
    def __init__(self, moves: Iterable[tuple[Vector, int]]):
        self.moves = list(moves)

    def part1(self) -> int:
        head = tail = Point(0, 0)
        visited = {tail}
        for head_move in self.get_steps():
            head = do_move(head, head_move)
            tail = get_following_position(head, tail)
            visited.add(tail)
        return len(visited)

    def part2(self) -> int:
        snake = [Point(0, 0) for _ in range(10)]
        visited = {Point(0, 0)}
        for head_move in self.get_steps():
            snake[0] = do_move(snake[0], head_move)
            for i in range(1, len(snake)):
                snake[i] = get_following_position(snake[i - 1], snake[i])
            visited.add(snake[-1])
        return len(visited)

    def get_steps(self) -> Iterable[Vector]:
        for move in self.moves:
            direction, count = move
            for _ in range(count):
                yield direction


def load_input(filename="input.txt") -> Day09:
    file = Path(__file__).with_name(filename)
    lines = file.read_text().splitlines()
    return Day09(map(parse_line, lines))


def parse_line(line: str) -> tuple[Vector, int]:
    words = line.split()
    direction = dirs[words[0]]
    count = int(words[1])
    return direction, count
