import unittest

from day01.day01 import read_input


class Day01TestCase(unittest.TestCase):
    def test_part1(self):
        sut = read_input()
        self.assertEqual(68442, sut.part1())

    def test_part2(self):
        sut = read_input()
        self.assertEqual(204837, sut.part2())


if __name__ == "__main__":
    unittest.main()
