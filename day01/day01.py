from pathlib import Path


class Day01:
    def __init__(self, data: dict[int, list[int]]):
        self.data = data

    @property
    def totals(self) -> list[int]:
        return list(map(sum, self.data.values()))

    def part1(self) -> int:
        return max(self.totals)

    def part2(self):
        top3 = sorted(self.totals)[-3:]
        return sum(top3)


def read_input(filename="input.txt") -> Day01:
    cals_by_elves = {}
    elf = 0
    for line in Path(__file__).with_name(filename).read_text().splitlines():
        if not line:
            elf += 1
            continue

        cals_by_elves[elf] = cals_by_elves.get(elf, set()).union([int(line)])

    return Day01(cals_by_elves)
