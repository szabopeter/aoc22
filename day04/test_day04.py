import unittest

from day04.day04 import is_full_overlap, load_input


class Day04TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(2, sut.part1())

    def test_overlap(self):
        for l1, h1, l2, h2, expected in [
            (3, 5, 4, 6, False),
            (4, 6, 3, 5, False),
            (3, 5, 4, 5, True),
            (4, 5, 3, 5, True),
            (3, 5, 6, 7, False),
            (6, 7, 3, 5, False),
            (3, 5, 2, 3, False),
            (2, 3, 3, 5, False),
            (3, 5, 3, 3, True),
            (3, 3, 3, 5, True),
        ]:
            with self.subTest(f"{l1}-{h1},{l2}-{h2}"):
                actual = is_full_overlap(((l1, h1), (l2, h2)))
                self.assertEqual(expected, actual)

    def test_part1(self):
        sut = load_input()
        self.assertNotEqual(354, sut.part1())
        self.assertNotEqual(636, sut.part1())
        self.assertEqual(509, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(4, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(870, sut.part2())


if __name__ == "__main__":
    unittest.main()
