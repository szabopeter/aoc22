from pathlib import Path
from typing import Iterable


class Day04:
    def __init__(
        self, assignments: Iterable[tuple[tuple[int, int], [tuple[int, int]]]]
    ):
        self.assignments = list(assignments)

    def part1(self) -> int:
        return sum(1 for ass in self.assignments if is_full_overlap(ass))

    def part2(self) -> int:
        return sum(1 for ass in self.assignments if is_partial_overlap(ass))


def is_full_overlap(assignment: tuple[tuple[int, int], tuple[int, int]]) -> bool:
    (l1, h1), (l2, h2) = sorted(assignment)
    assert l1 <= h1 and l2 <= h2
    assert l1 <= l2
    # result = l2 <= h1 and h2 <= h1
    result = l1 <= l2 <= h2 <= h1 or l2 <= l1 <= h1 <= h2
    # print(assignment, result)
    return result


def is_partial_overlap(assignment: tuple[tuple[int, int], tuple[int, int]]) -> bool:
    (l1, h1), (l2, h2) = sorted(assignment)
    assert l1 <= h1 and l2 <= h2
    assert l1 <= l2
    result = l1 <= l2 <= h1 or l2 <= l1 <= h2
    # print(assignment, result)
    return result


def load_input(filename="input.txt") -> Day04:
    file = Path(__file__).with_name(filename)
    lines = file.read_text().splitlines()
    return Day04(parse_line(line) for line in lines)


def parse_line(line: str) -> tuple[tuple[int, int], [tuple[int, int]]]:
    def parse_range(range_str: str) -> tuple[int, int]:
        low, high = list(map(int, range_str.split("-")))
        return low, high

    elf1, elf2 = line.split(",")
    return parse_range(elf1), parse_range(elf2)
