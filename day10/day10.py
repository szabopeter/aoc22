from pathlib import Path
from typing import Iterable


class Day10:
    def __init__(self, instructions: Iterable[list]):
        self.instructions = list(instructions)

    def part1(self) -> int:
        measurement_points = [20, 60, 100, 140, 180, 220]
        max_pt = max(measurement_points)
        history = []
        for cycle_number, x in enumerate(self.simulate()):
            history.append(x)
            if cycle_number > max_pt:
                break
        signal_strengths = {pt * history[pt - 1] for pt in measurement_points}
        return sum(signal_strengths)

    def part2(self) -> list[str]:
        def advance_crt(r, c):
            c += 1
            if c >= 40:
                c = 0
                r += 1
            return r, c

        row, col = 0, 0
        output = {}
        for x in self.simulate():
            ch = "#" if abs(col - x) <= 1 else "."
            output[row] = output.get(row, "") + ch
            row, col = advance_crt(row, col)
        return list(output.values())

    def simulate(self) -> Iterable[int]:
        x = 1
        for instruction in self.instructions:
            cmd = instruction[0]
            if cmd == "noop":
                yield x
            elif cmd == "addx":
                delta_x = instruction[1]
                yield x
                yield x
                x += delta_x
            else:
                raise ValueError(f"Couldn't process {instruction}")


def load_input(filename="input.txt") -> Day10:
    file = Path(__file__).with_name(filename)
    lines = file.read_text().splitlines()
    return Day10(map(parse_line, lines))


def parse_line(line: str) -> list:
    words = line.split()
    if words[0] == "noop":
        assert len(words) == 1
        return ["noop"]
    if words[0] == "addx":
        assert len(words) == 2
        return ["addx", int(words[1])]
    raise ValueError(f"Couldn't parse {line}")


def load_output(filename="output.txt") -> list[str]:
    file = Path(__file__).with_name(filename)
    return file.read_text().splitlines()
