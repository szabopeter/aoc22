import unittest

from day10.day10 import Day10, load_input, load_output


class Day10TestCase(unittest.TestCase):
    def test_dummy(self):
        sut = Day10(
            [
                ["noop"],
                ["addx", 3],
                ["addx", -5],
            ]
        )
        actual = list(sut.simulate())
        self.assertEqual([1, 1, 1, 4, 4], actual)

    def test_example_mpts(self):
        sut = load_input("example.txt")
        history = list(sut.simulate())
        mvals = [history[mp - 1] for mp in [20, 60, 100, 140, 180, 220]]
        self.assertEqual([21, 19, 18, 21, 16, 18], mvals)

    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(13140, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(14360, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        expected = load_output("example_output.txt")
        self.assertEqual(expected, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(load_output(), sut.part2())


if __name__ == "__main__":
    unittest.main()
