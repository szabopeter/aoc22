import unittest

from day07.day07 import load_input


class Day07TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(95437, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertNotEqual(1272914, sut.part1())
        self.assertEqual(1644735, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(24933642, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(1300850, sut.part2())


if __name__ == "__main__":
    unittest.main()
