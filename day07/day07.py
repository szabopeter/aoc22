from functools import cache
from pathlib import Path


class Day07:
    def __init__(self, dirs: dict[tuple[str, ...], dict[str, int]]):
        self.dirs = {path: sum(dir_dict.values())
                     for path, dir_dict in dirs.items()}

    @cache
    def calc_recursize_path_size(self, prefix: tuple[str, ...]) -> int:
        matches = {
            path: dir_size
            for path, dir_size in self.dirs.items()
            if path[: len(prefix)] == prefix
        }
        total = sum(matches.values())
        return total

    def part1(self) -> int:
        dir_sizes = {
            prefix: self.calc_recursize_path_size(prefix) for prefix in self.dirs
        }
        return sum(size for size in dir_sizes.values() if size <= 100000)

    def part2(self) -> int:
        dir_sizes = {
            prefix: self.calc_recursize_path_size(prefix) for prefix in self.dirs
        }
        disk_size = 70000000
        used = dir_sizes[("/",)]
        unused = disk_size - used
        required = 30000000
        delete_at_least = required - unused
        return min(size for size in dir_sizes.values() if size >= delete_at_least)


def load_input(filename="input.txt") -> Day07:
    file = Path(__file__).with_name(filename)
    lines = file.read_text().splitlines()
    dirs: dict[tuple[str, ...], dict[str, int]] = {("/",): {}}
    cwd = ()
    for line in lines:
        if line.startswith("$"):
            command = line.split()[1:]
            if command[0] == "cd":
                if command[1] == "..":
                    cwd = cwd[:-1]
                else:
                    cwd = tuple(list(cwd) + [command[1]])
            else:
                assert line == "$ ls", f"Expected '$ ls', was {line}"
            continue

        # ls
        size_str, filename = line.split()
        if size_str == "dir":
            dirs[tuple(list(cwd) + [filename])] = {}
            continue

        size = int(size_str)
        file_dir = dirs[cwd]
        file_dir[filename] = size
        dirs[cwd] = file_dir

    return Day07(dirs)
