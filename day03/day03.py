from pathlib import Path
from typing import Iterable


class Rucksack:
    def __init__(self, comp1_items: str, comp2_items: str):
        self.comp1_items = set(comp1_items)
        self.comp2_items = set(comp2_items)

    def get_common_item(self) -> str:
        common_items = self.comp1_items.intersection(self.comp2_items)
        assert len(common_items) == 1
        return common_items.pop()

    def get_items(self) -> set[str]:
        return self.comp1_items.union(self.comp2_items)


class Day03:
    def __init__(self, rucksacks: Iterable[Rucksack]):
        self.rucksacks = rucksacks

    def part1(self) -> int:
        return sum(
            get_item_priority(rucksack.get_common_item()) for rucksack in self.rucksacks
        )

    def part2(self) -> int:
        return sum(
            get_item_priority(get_badge_item(group)) for group in self.get_groups()
        )

    def get_groups(self) -> Iterable[list[Rucksack]]:
        group = []
        for rucksack in self.rucksacks:
            group.append(rucksack)
            if len(group) == 3:
                yield group[:]
                group.clear()
        assert not group


def parse_line(line: str) -> Rucksack:
    assert len(line) % 2 == 0
    half_index = len(line) // 2
    comp1_items = line[:half_index]
    comp2_items = line[half_index:]
    return Rucksack(comp1_items, comp2_items)


def get_item_priority(item: str) -> int:
    assert len(item) == 1
    letter = item[0]
    assert letter.isalpha()
    if letter.islower():
        return ord(letter) - ord("a") + 1
    return ord(letter) - ord("A") + 27


def get_badge_item(group: list[Rucksack]) -> str:
    assert len(group) == 3
    common_items = group.pop().get_items()
    while group:
        common_items.intersection_update(group.pop().get_items())
    assert len(common_items) == 1
    return common_items.pop()


def load_input(filename="input.txt") -> Day03:
    file = Path(__file__).with_name(filename)
    lines = file.read_text().splitlines()
    return Day03(parse_line(line) for line in lines)
