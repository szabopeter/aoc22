import unittest

from day03.day03 import load_input


class Day03TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(157, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(7903, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(70, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(2548, sut.part2())


if __name__ == "__main__":
    unittest.main()
