import datetime
import logging
import shutil
import sys
from pathlib import Path
from typing import Union


def get_day(argv: list[str]) -> str:
    # remove script name
    argv.pop(0)
    if argv:
        day_str = argv[0]
        is_valid = is_valid_day_arg(day_str)
        if is_valid:
            return day_str.zfill(2)
        raise ValueError(
            "Invalid args, you must specify the day as a 1 <= number <= 31 (but it's optional)"
        )

    today = datetime.datetime.today()
    return f"{today.day:02}"


def is_valid_day_arg(day_str: str) -> bool:
    try:
        day_num = int(day_str)
        return 1 <= day_num <= 31
    except ValueError:
        pass
    return False


def main(argv: list[str]):
    day = get_day(argv)
    template_dir = Path(__file__).with_name("dayXX")
    assert template_dir.is_dir()
    target_dir = Path(__file__).with_name(f"day{day}")
    assert not target_dir.exists(), f"{target_dir} already exists!"
    templater = Templater(
        {
            "XX": day,
            "Xx": day,
        }
    )
    shutil.copytree(
        template_dir, target_dir, copy_function=templater.copy, ignore=get_ignored_names
    )


def get_ignored_names(_src: str, _names: list[str]) -> list[str]:
    return ["__pycache__"]


class Templater:
    def __init__(self, replacements: dict[str, str]):
        self.replacements = replacements

    def patch(self, content: str) -> str:
        for search, replace in self.replacements.items():
            content = content.replace(search, replace)
        return content

    def copy(self, source: Union[Path, str], destination: Union[Path, str]):
        destination = self.patch(destination)
        logging.info("Copying %s to %s", source, destination)
        source = Path(source)
        destination = Path(destination)
        encoding = "utf-8"
        content = source.read_text(encoding=encoding)
        content = self.patch(content)
        destination.write_text(content, encoding=encoding)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main(sys.argv)
