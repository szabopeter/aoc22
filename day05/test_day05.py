import unittest

from day05.day05 import load_input


class Day05TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual("CMZ", sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual("CFFHVVHNC", sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual("MCD", sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual("FSZWBPTBG", sut.part2())


if __name__ == "__main__":
    unittest.main()
