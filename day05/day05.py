from pathlib import Path
from typing import Iterable


class Day05:
    def __init__(self, moves: Iterable[tuple[int, int, int]]):
        self.stacks: dict[int, list[str]] = {}
        self.moves = list(moves)

    def part1(self) -> str:
        for count, src, dst in self.moves:
            for i in range(count):
                self.move1(src, dst)
        return self.tops()

    def part2(self) -> str:
        for count, src, dst in self.moves:
            self.move(src, dst, count)
        return self.tops()

    def push(self, stack_nr: int, crates: Iterable[str]):
        for crate in crates:
            if stack_nr not in self.stacks:
                self.stacks[stack_nr] = []
            self.stacks[stack_nr].append(crate)

    def move1(self, src: int, dst: int):
        crate = self.stacks[src].pop()
        self.stacks[dst].append(crate)

    def tops(self) -> str:
        return "".join(self.stacks[i][-1] for i in sorted(self.stacks.keys()))

    def move(self, src: int, dst: int, count: int):
        crates = []
        for i in range(count):
            crates.append(self.stacks[src].pop())
        for crate in reversed(crates):
            self.stacks[dst].append(crate)


def load_input(filename="input.txt") -> Day05:
    file = Path(__file__).with_name(filename)
    lines = file.read_text().splitlines()
    starting_lines = []
    line = lines.pop(0)
    while line:
        starting_lines.append(line)
        line = lines.pop(0)
    state = Day05(parse_line(line) for line in lines)
    stack_count = max(int(nr) for nr in starting_lines.pop().split())
    for i in range(stack_count):
        stack_nr = i + 1
        for j in range(len(starting_lines) - 1, -1, -1):
            try:
                crate = starting_lines[j][1 + i * 4]
            except IndexError:
                continue

            if crate == " ":
                continue

            state.push(stack_nr, crate)
    return state


def parse_line(line: str) -> tuple[int, int, int]:
    pts = line.split()
    return int(pts[1]), int(pts[3]), int(pts[5])


def main():
    state = load_input()
    print(state.part1())

    state = load_input()
    print(state.part2())


if __name__ == "__main__":
    main()
