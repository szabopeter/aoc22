from pathlib import Path
from typing import Callable

dirs = {(-1, 0), (1, 0), (0, -1), (0, 1)}


class Day08:
    def __init__(self, hmap: dict[tuple[int, int], int]):
        self.hmap = hmap
        self.w = max(x for x, _ in hmap) + 1
        self.h = max(y for _, y in hmap) + 1

    def part1(self) -> int:
        visibles: set[tuple[int, int]] = set()
        for y in range(self.h):
            lmax = -1
            for x in range(self.w):
                h = self.hmap[x, y]
                if h > lmax:
                    visibles.add((x, y))
                    lmax = h
        for y in range(self.h):
            lmax = -1
            for x in reversed(range(self.w)):
                h = self.hmap[x, y]
                if h > lmax:
                    visibles.add((x, y))
                    lmax = h
        for x in range(self.w):
            lmax = -1
            for y in range(self.h):
                h = self.hmap[x, y]
                if h > lmax:
                    visibles.add((x, y))
                    lmax = h
        for x in range(self.w):
            lmax = -1
            for y in reversed(range(self.h)):
                h = self.hmap[x, y]
                if h > lmax:
                    visibles.add((x, y))
                    lmax = h
        return len(visibles)

    def part2(self) -> int:
        scores: dict[tuple[int, int], list[int]] = {}
        for y in range(self.h):
            hs = []
            for x in range(self.w):
                h = self.hmap[x, y]
                i = 0
                while i < len(hs) and hs[len(hs) - 1 - i] < h:
                    i += 1
                if i < len(hs):
                    i += 1
                scores[x, y] = scores.get((x, y), []) + [i]
                hs.append(h)
        for y in range(self.h):
            hs = []
            for x in reversed(range(self.w)):
                h = self.hmap[x, y]
                i = 0
                while i < len(hs) and hs[len(hs) - 1 - i] < h:
                    i += 1
                if i < len(hs):
                    i += 1
                scores[x, y] = scores.get((x, y), []) + [i]
                hs.append(h)
        for x in range(self.w):
            hs = []
            for y in range(self.h):
                h = self.hmap[x, y]
                i = 0
                while i < len(hs) and hs[len(hs) - 1 - i] < h:
                    i += 1
                if i < len(hs):
                    i += 1
                scores[x, y] = scores.get((x, y), []) + [i]
                hs.append(h)
        for x in range(self.w):
            hs = []
            for y in reversed(range(self.h)):
                h = self.hmap[x, y]
                i = 0
                while i < len(hs) and hs[len(hs) - 1 - i] < h:
                    i += 1
                if i < len(hs):
                    i += 1
                scores[x, y] = scores.get((x, y), []) + [i]
                hs.append(h)

        def get_ss(p: tuple[int, int]) -> int:
            sl = scores[p]
            return sl[0] * sl[1] * sl[2] * sl[3]

        return max(map(get_ss, scores.keys()))

    def get_nmap(self, d: tuple[int, int]) -> dict[tuple[int, int], int]:
        dx, dy = d
        return {
            (x, y): 1 if self.hmap[x, y] > self.hmap.get((x + dx, y + dy), -1) else 0
            for x, y in self.hmap
        }


class GenMap:
    def __init__(
        self,
        pdep: Callable[[tuple[int, int]], tuple[int, int]],
        transformation: Callable[[int], int],
    ):
        self.gmap: dict[tuple[int, int], int] = {}
        self.pdep = pdep
        self.transformation = transformation

    def get(self, a: tuple[int, int]) -> int:
        b = self.pdep(a)
        v = self.get(b)
        self.gmap[a] = self.transformation(v)
        return v


def load_input(filename="input.txt") -> Day08:
    file = Path(__file__).with_name(filename)
    lines = file.read_text().splitlines()
    hmap = {
        (x, y): int(ch) for (y, line) in enumerate(lines) for (x, ch) in enumerate(line)
    }
    return Day08(hmap)
