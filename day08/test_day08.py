import unittest

from day08.day08 import load_input


class Day08TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(21, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(1543, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(8, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(595080, sut.part2())


if __name__ == "__main__":
    unittest.main()
