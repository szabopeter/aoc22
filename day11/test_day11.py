import logging
import unittest

from day11.day11 import load_input


class Day11TestCase(unittest.TestCase):
    def test_part1_example(self):
        logging.basicConfig(level=logging.DEBUG)
        sut = load_input("example.txt")
        self.assertEqual(10605, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(62491, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt", can_div=False)
        self.assertEqual(2713310158, sut.part2())

    def test_part2(self):
        sut = load_input(can_div=False)
        self.assertEqual(17408399184, sut.part2())


if __name__ == "__main__":
    unittest.main()
