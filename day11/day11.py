import logging
from math import lcm
from pathlib import Path
from typing import Callable, Iterable


def log(*args):
    logging.getLogger("details").debug(*args)


class Monkey:
    def __init__(self, nr: int, items: list[int], op: Callable[[int], int], divby: int, on_true: int, on_false_: int):
        self.nr = nr
        self.items = items
        self.op = op
        self.divby = divby
        self.on_true = on_true
        self.on_false = on_false_
        self.inspections = 0


class Day11:
    def __init__(self, monkeys: Iterable[Monkey]):
        self.monkeys = list(monkeys)

    def part1(self) -> int:
        return self.calculate_monkey_business(True, 20)

    def part2(self) -> int:
        return self.calculate_monkey_business(False, 10000)

    def calculate_monkey_business(self, can_div: int, rounds: int) -> int:
        lcd = lcm(*(monkey.divby for monkey in self.monkeys))

        for round_nr in range(rounds):
            for monkey in self.monkeys:
                log("Monkey %d:", monkey.nr)
                while monkey.items:
                    worry = monkey.items.pop(0)
                    log("  Monkey inspects an item with a worry level of %d.", worry)
                    monkey.inspections += 1
                    worry = monkey.op(worry) % lcd
                    log("  Worry level increased to %d.", worry)
                    if can_div:
                        worry = worry // 3
                        log("  Monkey gets bored with item. Worry level is divided by 3 to %d.", worry)
                    is_divisible = worry % monkey.divby == 0
                    is_divisible_str = 'is' if is_divisible else 'is not'
                    log("  Current worry level %s divisible by 19.", is_divisible_str)
                    target_monkey = monkey.on_true if is_divisible else monkey.on_false
                    log("  Item with worry level %d is thrown to monkey %d.",
                        worry, target_monkey)
                    self.monkeys[target_monkey].items.append(worry)

            log(f"After round %d, the monkeys are holding items with these worry levels:", round_nr + 1)
            for monkey in self.monkeys:
                items = ', '.join(map(str, monkey.items))
                log("Monkey %d: %s", monkey.nr, items)
            log("")
        for monkey in self.monkeys:
            logging.debug("Monkey %d inspected items %d times.",
                          monkey.nr, monkey.inspections)
        inspections = (monkey.inspections for monkey in self.monkeys)
        inspections = sorted(inspections, reverse=True)
        return inspections[0] * inspections[1]


def load_input(filename="input.txt", can_div=True) -> Day11:
    file = Path(__file__).with_name(filename)
    paras = file.read_text().split("\n\n")
    assert paras, f"{filename} is empty!"
    return Day11(parse_monkey_lines(para.splitlines()) for para in paras)


def parse_monkey_lines(lines: list[str]) -> Monkey:
    monkey_nr = int(lines[0][-2])
    items = [int(item) for item in lines[1].split(": ")[1].split(", ")]
    operation_text = lines[2].split(": ")[1].replace("new = ", "lambda old: ")
    operation = eval(operation_text)
    divby = int(lines[3].split("divisible by ")[1])
    assert "If true" in lines[4]
    on_true = int(lines[4].split("to monkey ")[1])
    assert "If false" in lines[5]
    on_false = int(lines[5].split("to monkey")[1])
    return Monkey(monkey_nr, items, operation, divby, on_true, on_false)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("details").setLevel(logging.INFO)

    print("Part1: ", load_input().part1())
    print("Part2: ", load_input().part2())
