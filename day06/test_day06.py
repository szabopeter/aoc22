import unittest

from day06.day06 import Day06, load_input


class Day06TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(7, sut.part1())

    def test_part1_examples(self):
        for text, expected in [("bvwbjplbgvbhsrlpgdmjqwftvncz", 5)]:
            with self.subTest(text):
                self.assertEqual(expected, Day06(text).part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(1848, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(19, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(2308, sut.part2())


if __name__ == "__main__":
    unittest.main()
