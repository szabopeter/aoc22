from pathlib import Path


def find_first_marker(text: str, buflen: int) -> int:
    for i in range(buflen, len(text)):
        bufstart = i - buflen
        if len(set(text[bufstart:i])) == buflen:
            return i
    raise ValueError("Marker not found!")


class Day06:
    def __init__(self, text: str):
        self.text = text

    def part1(self) -> int:
        return find_first_marker(self.text, 4)

    def part2(self) -> int:
        return find_first_marker(self.text, 14)


def load_input(filename="input.txt") -> Day06:
    file = Path(__file__).with_name(filename)
    return Day06(file.read_text().strip())
