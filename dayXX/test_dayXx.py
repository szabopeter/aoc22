import unittest

from dayXX.dayXx import load_input


@unittest.SkipTest
class DayXxTestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(-1, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(-2, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(-3, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(-4, sut.part2())


if __name__ == "__main__":
    unittest.main()
