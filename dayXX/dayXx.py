from pathlib import Path


class DayXx:
    def __init__(self):
        pass

    def part1(self) -> int:
        pass

    def part2(self) -> int:
        pass


def load_input(filename="input.txt") -> DayXx:
    file = Path(__file__).with_name(filename)
    lines = file.read_text().splitlines()
    assert lines, f"{filename} is empty!"
    return DayXx()
