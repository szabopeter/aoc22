from pathlib import Path
from typing import Callable, Iterable, Optional

from shared.point import Point, Vector


class Line:
    def __init__(self, waypts: Iterable[Vector]):
        """
            >>> set(Line([Point(498, 4), Point(498, 6)]).get_all_pts()) == {Point(498, 4), Point(498, 5), Point(498, 6)}
            True
            >>> {pt for pt in Line([Point(5, -3), Point(5, -5)])} == {Point(5, -3), Point(5, -4), Point(5, -5)}
            True
        """
        self.waypts = list(waypts)

    def __contains__(self, item: Vector) -> bool:
        return item in self.get_all_pts()

    def get_all_pts(self) -> Iterable[Vector]:
        for a, b in zip(self.waypts[:-1], self.waypts[1:]):
            v = b - a
            u = v.unit()
            p = a
            yield a
            while p != b:
                p += u
                yield p

    def __iter__(self) -> Iterable[Vector]:
        yield from self.get_all_pts()


class Day14:
    def __init__(self, lines: Iterable[Line]):
        self.lines = list(lines)
        self.resting = {pt for line in self.lines for pt in line}
        self.initials = self.resting.copy()

    def part1(self) -> int:
        return self.common(None)

    def part2(self) -> int:
        base_y = max(p.y for p in self.resting) + 2
        return self.common(base_y)

    def common(self, base_y: Optional[int]) -> int:
        start = Point(500, 0)
        path = [start]
        while 0 < len(path) < 1000:
            p = path[-1]
            nxt = self.make_fall(p, base_y)
            if isinstance(nxt, Vector):
                # can fall
                path.append(nxt)
                continue

            # is a rest pos
            self.resting.add(p)
            path = path[:-1]

        return len(self.resting.difference(self.initials))

    def make_fall(self, p: Vector, base_y: Optional[int]) -> Optional[Vector]:
        is_based: Callable[[Vector], bool] = (
            lambda _: False) if base_y is None else lambda n: n.y >= base_y

        for d in (Point(0, 1), Point(-1, 1), Point(1, 1)):
            assert isinstance(p, Vector)
            assert isinstance(d, Vector)
            nxt = p + d
            if not is_based(nxt) and self.is_free(nxt):
                return nxt

    def is_free(self, p: Vector) -> bool:
        return p not in self.resting


def load_input(filename="input.txt") -> Day14:
    file = Path(__file__).with_name(filename)
    text_lines = file.read_text().splitlines()

    assert text_lines, f"{filename} is empty!"
    return Day14(map(parse_line, text_lines))


def parse_line(text_line: str) -> Line:
    pts = map(parse_word, text_line.split(" -> "))
    return Line(pts)


def parse_word(word: str) -> Vector:
    x, y = [int(x) for x in word.split(",")]
    return Point(x, y)
