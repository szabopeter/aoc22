import unittest

from day14.day14 import load_input


class Day14TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(24, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(897, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(93, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(26683, sut.part2())


if __name__ == "__main__":
    unittest.main()
