import unittest

from day13.day13 import load_input


class Day13TestCase(unittest.TestCase):
    def test_part1_example(self):
        sut = load_input("example.txt")
        self.assertEqual(1 + 2 + 4 + 6, sut.part1())

    def test_part1(self):
        sut = load_input()
        self.assertEqual(5882, sut.part1())

    def test_part2_example(self):
        sut = load_input("example.txt")
        self.assertEqual(140, sut.part2())

    def test_part2(self):
        sut = load_input()
        self.assertEqual(24948, sut.part2())


if __name__ == "__main__":
    unittest.main()
