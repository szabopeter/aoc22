from functools import cmp_to_key
from pathlib import Path
from typing import Iterable, Union


def list_compare(left: Union[list, int], right: Union[list, int]) -> int:
    if left == right:
        return 0

    if isinstance(left, int) and isinstance(right, int):
        if left < right:
            return 1
        if left > right:
            return -1

    left = left if isinstance(left, list) else [left]
    right = right if isinstance(right, list) else [right]
    for left_item, right_item in zip(left, right):
        cmp = list_compare(left_item, right_item)
        if cmp != 0:
            return cmp

    if len(left) < len(right):
        return 1

    if len(left) > len(right):
        return -1

    return 0


class Day13:
    def __init__(self, pairs: Iterable[tuple[list, list]]):
        self.pairs = list(pairs)

    def part1(self) -> int:
        return sum([i+1 for i, (left, right) in enumerate(self.pairs) if list_compare(left, right) == 1])

    def part2(self) -> int:
        singles = [pair[i] for pair in self.pairs for i in (0, 1)] + [
            [[2]],
            [[6]]
        ]
        singles.sort(key=cmp_to_key(list_compare), reverse=True)
        i1 = singles.index([[2]]) + 1
        i2 = singles.index([[6]]) + 1
        return i1 * i2


def load_input(filename="input.txt") -> Day13:
    file = Path(__file__).with_name(filename)
    paras = file.read_text().split("\n\n")
    assert paras, f"{filename} is empty!"
    return Day13(map(parse_para, paras))


def parse_para(para: str) -> tuple[list, list]:
    lines = para.splitlines()
    assert len(lines) == 2
    assert lines[0] != lines[1]
    p1 = eval(lines[0])
    p2 = eval(lines[1])
    assert isinstance(p1, list)
    assert isinstance(p2, list)
    return p1, p2
